packages
========

Requirements
------------

This role requires Ansible 1.4 or higher, and platform requirements are listed
in the metadata file.

Examples
--------

Install nrpe and set the default settings.

        - hosts: all
          roles:
            - role: nrpe
