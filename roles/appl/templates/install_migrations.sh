#!/bin/bash -x
#########################################################################
## Working Directory: /home/cpt/paxcel/components 
## Dependencies: git Percona-Server-devel-56
#########################################################################

gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3
\curl -sSL https://get.rvm.io | bash -s stable
source ~/.profile 
rvm install 2.1
gem install bundler
rvm reload
cd /home/cpt/paxcel/components/migrations
rvm all do bundle install
touch /tmp/migrations
