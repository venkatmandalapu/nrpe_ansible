#!/bin/bash
###############################################################################
#  Description: Script fetches Percona db backup from File Server             #
#                                                                             #
###############################################################################
BACKUP_SERVER='172.31.90.80'
USER='cpt'
BACKUP_DIR='/home/cpt/backup'
#LOCAL_DIR='/home/cpt/data'
LOCAL_DIR='/var/lib/mysql'

###############################################################################
function abort(){
  echo $1
  exit 1
}

function find_folder_with_latest_files(){
  CURRENT_TIME=$(date +"%H")
  if [[ $CURRENT_TIME -le '1' ]]; then
    DAY=$(date +"%a" -d "yesterday")
  else
    DAY=$(date +"%a")
  fi
  echo $DAY
}

function fetch_backup(){
  OPTIONS='-aze ssh -t'
  DAY=$(find_folder_with_latest_files)

  sudo rm -f $LOCAL_DIR/*
  sudo rsync $OPTIONS $USER@$BACKUP_SERVER:$BACKUP_DIR/$DAY/ $LOCAL_DIR/
  [[ $? -ne 0 ]] && abort "[ERROR] Failed to fetch backup from file server."
  echo "[INFO] Percona backup fetched succesfully."
}

function restore_db(){

  sudo service mysql stop

  echo "[INFO] Moving data to mysql directory..."
  #sudo mv $LOCAL_DIR/* /var/lib/mysql/
  [[ $? -ne 0 ]] && abort "[ERROR] Failed to move data to mysql directory"

  echo "[INFO] Restoring db dumps..."
  sudo innobackupex --apply-log /var/lib/mysql/
  [[ $? -ne 0 ]] && abort "[ERROR] Execution of innobackupex failed."

  sudo chown -R mysql:mysql /var/lib/mysql/

  sudo service mysql start
  [[ $? -ne 0 ]] && abort "[ERROR] Failed to start Mysql service."
  echo "[INFO] Percona DB restored succesfully."
}

### MAIN ###
fetch_backup
restore_db
exit 0
