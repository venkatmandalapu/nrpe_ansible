#!/bin/bash

MYSQL_USER=cpt
MYSQL_PASSWORD='**CPT$$cpt**'
MYSQL_SRC_HOST=172.31.90.80
MYSQL_LOCAL_HOST=127.0.0.1
MYSQL_DB=cpt
BACKUP_SERVER=172.31.90.16

function usage(){
  echo "Usage: $0 -s <start-date> -e <end-date>"
  echo "date format: [YYYY-MM-DD]" 1>&2; exit 1;
}

function check_dates(){
  USER_FORMAT=$1
  EXPECTED_FORMAT=$(date -d "$1" +"%Y-%m-%d")
  echo "EXPECTED_FORMAT: $EXPECTED_FORMAT"
  echo "USER_FORMAT: $USER_FORMAT"
  [[ "$EXPECTED_FORMAT" -ne "$USER_FORMAT" ]] && usage
}

function refresh_table() {
### local variables
 table_name=$1
 date_column_name=$2
 csv_file=${table_name}_${END_DATE}.csv

### Export_table_as_csv
 mysql -u$MYSQL_USER -p$MYSQL_PASSWORD -h$MYSQL_SRC_HOST $MYSQL_DB -Bse \
 "
 SELECT * FROM $table_name WHERE $date_column_name >= '${START_DATE}' and $date_column_name <= '${END_DATE}'
 INTO OUTFILE '/tmp/$csv_file'
 FIELDS TERMINATED BY ','
 ENCLOSED BY '"\""'
 LINES TERMINATED BY '\n';
 "
## Copy table to local server
 rsync -aze ssh -t cpt@$MYSQL_SRC_HOST:/tmp/$csv_file /tmp/

### Delete old entries in table
 mysql -u$MYSQL_USER -p$MYSQL_PASSWORD $MYSQL_DB -Bse \
 "
 DELETE FROM  $table_name WHERE $date_column_name >= '${START_DATE}' and $date_column_name <= '${END_DATE}';
 "
### Import table data
 mysql -u$MYSQL_USER -p$MYSQL_PASSWORD $MYSQL_DB -Bse \
 "
 LOAD DATA LOCAL INFILE '/tmp/$csv_file'
 INTO TABLE $table_name
 FIELDS TERMINATED BY ','
 ENCLOSED BY '"\""'
 LINES TERMINATED BY '\n';
 "
}

function fetch_serialize_files() {
 DAY=$(date +"%a")
 FIRST_FILE=$(ssh cpt@$BACKUP_SERVER ls -ht /home/cpt/chaikin_backups/data/$DAY/data | head -n 1)
 LATEST_FILES=$(ssh cpt@$BACKUP_SERVER ls -ht /home/cpt/chaikin_backups/data/$DAY/data | head -n 2)
 SECOND_FILE=`echo ${LATEST_FILES#$FIRST_FILE} |  tr -d '\n'`

 echo "Fetching files: $FIRST_FILE, $SECOND_FILE"

 mkdir -p /home/cpt/paxcel/components/pgrData
 scp cpt@$BACKUP_SERVER:/home/cpt/chaikin_backups/data/$DAY/pgrSerializedData.ser /home/cpt/paxcel/components/pgrData
 [[ $? -ne 0 ]] && echo "Failed to fetch pgrSerializedData.ser file."

 scp cpt@$BACKUP_SERVER:/home/cpt/chaikin_backups/data/$DAY/data/$FIRST_FILE /home/cpt/paxcel/components/pgrData
 [[ $? -ne 0 ]] && echo "Failed to fetch $FIRST_FILE file."

 scp cpt@$BACKUP_SERVER:/home/cpt/chaikin_backups/data/$DAY/data/$SECOND_FILE /home/cpt/paxcel/components/pgrData
 [[ $? -ne 0 ]] && echo "Failed to fetch $SECOND_FILE file."
}

### M A I N ###

if [[($# == 0) || ($# == 2)]]; then
  usage
fi

while getopts "s:e:" o; do
 case "${o}" in
     s)
         START_DATE=${OPTARG}
         check_dates $START_DATE
         ;;
     e)
         END_DATE=${OPTARG}
         check_dates $END_DATE
         ;;
     *)
         usage
         ;;
 esac
done

refresh_table 'pgr_ranks_data_otc' 'pgr_date'
refresh_table 'pgr_rank_data_current' 'pgr_date'
refresh_table 'pgr_rank_data_current_archive' 'pgr_date'
refresh_table 'raw_premium_data' 'date'
refresh_table 'premium_data_computations' 'calculations_date'
refresh_table 'premium_data_signals' 'date'
fetch_serialize_files
